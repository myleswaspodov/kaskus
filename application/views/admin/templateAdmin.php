<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Cyberleech Auto Kaskus</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/style.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-11.2.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('#strip').dataTable( {
				"order": [[ 0, "desc" ]]
			} );
		} );
	</script>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
		
          <a class="navbar-brand" href="#">CloudMyFile Auto Kaskus</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Menu <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?php echo base_url()?>admin/user">User</a></li>
                <li><a href="<?php echo base_url()?>admin/thread">Thread</a></li>
				<li><a href="<?php echo base_url()?>admin/sundulCmf">Sundul</a></li>
              </ul>
            </li>
          </ul>
          
        </div><!--/.nav-collapse -->
	  </div>
    </nav>
	
	<div class="container theme-showcase" role="main">
		<?php $this->load->view($isi) ?>
	</div>
<script>
	$('#reset').click(function(){
		$('#linkthread').val("");
	});
</script>
</body>
</html>
