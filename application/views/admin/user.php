<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
<div class="container">
		  <h1>Pengguna Kaskus</h1>
		  <ul class="nav nav-tabs">
			<li class="active"><a href="http://cloudmyfile.com/kaskus/admin/user">Manajemen Pengguna</a></li>
		  </ul>
</div>
<div class="jumbotron">
		<form method="post" action="<?php echo base_url() ?>admin/simpanUser">
			<table class="table" border ="0">
			<p>Tambah Pengguna</p>
			<tr>
				<td align="center">Username</td><td><input type="text" name="username" id="username" class="form-control" placeholder = "" required></td>
			</tr>
			<tr>
				<td align="center">Password</td><td><input type="text" name="password" id="password" class="form-control" placeholder = "" required></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
		</div>
		
		<div class="jumbotron">
		<h4>Daftar User</h4>
		<hr>
		<table class='table table-striped table-hover table-bordered table-responsive bordered' id='strip'>
		<thead style='background:#000;color:#fff'>
			<tr>
				<th style="background:black;color:white;" width="25">User Id</th>
				<th style="background:black;color:white;" width="100">Username</th>
				<th style="background:black;color:white;" width="250">Cookie</th>
				<th style="background:black;color:white;" width="25">Action</th>
			</tr>
		</thead>
			<tbody>
				<?php $no = 1; foreach($user as $us){ ?>
				<tr>
					
					 <td><?php echo $us['user_id']; ?></td>
					 <td><?php echo $us['username']; ?></td>	
					 <td><?php echo $us['cookie']; ?></td>
					 <td>
						 <a class="btn btn-danger"  href="<?php echo base_url() ?>admin/deleteUser/<?php echo $us['user_id'] ?>" onclick="return confirm('anda yakin akan hapus User <?php echo $us['username'] ?> ?')"><i class='glyphicon glyphicon-trash'></i> </a>
					 </td>				 
				</tr>
				<?php  } ?>
		
			</tbody>
		</table>
		
		</div>