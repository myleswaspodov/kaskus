<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
<div class="jumbotron">
		<form method="post" action="<?php echo base_url() ?>admin/simpanThread">
			<table class="table" border ="0">
			<tr>
				<td align="center">Link Thread</td><td><input type="text" name="linkthread" id="linkthread" class="form-control" placeholder = "" required></td>
			</tr>
			<tr>
				<td align="center">Keterangan</td><td><input type="text" name="keterangan" id="keterangan" class="form-control" placeholder = "" required></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Save</button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
		</div>
		
		<div class="jumbotron">
		<h4>Daftar Link Thread</h4>
		<hr>
		<table class='table table-striped table-hover table-bordered table-responsive bordered' id='strip'>
		<thead style='background:#000;color:#fff'>
			<tr>
				<th>Thread Id</th>
				<th>Link Thread</th>
				<th>Keterangan</th>
				<th>Action</th>
			</tr>
		</thead>
			<tbody>
				<?php $no = 1; foreach($thread as $tr){ ?>
				<tr>
					 <td><?php echo $tr['thread_id']; ?></td>
					 <td><?php echo $tr['thread_link']; ?></td>	
					 <td><?php echo $tr['keterangan']; ?></td>
					 <td><a class="btn btn-primary" href="<?php echo base_url() ?>admin/editThread/<?php echo $tr['thread_id'] ?>"><i class='glyphicon glyphicon-pencil'></i> Edit</a> &nbsp;&nbsp;
						 <a class="btn btn-danger"  href="<?php echo base_url() ?>admin/deleteThread/<?php echo $tr['thread_id'] ?>" onclick="return confirm('anda yakin akan hapus Thread <?php echo $tr['thread_link'] ?> ?')"><i class='glyphicon glyphicon-remove'></i> Delete
					 </td>				 
				</tr>
				<?php $no++; } ?>
		
			</tbody>
		</table>
		
		</div>