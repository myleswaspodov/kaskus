<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/style.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-11.2.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Login Otomasi Kaskus</h1>
            <div class="account-wall">
				<?php if($error){ ?>
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>Username dan Password Tidak Sesuai!!!</strong>
					</div>
				<?php }?>
                <img class="profile-img" src="http://i.ytimg.com/vi/YoRIwXKsFBc/maxresdefault.jpg" alt="">
				
                <form class="form-signin" method="post" action="<?php echo base_url() ?>otomasi/login">
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required autofocus>
                <input type="password" class="form-control" name="pass" id="pass" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Sign in</button>
                <label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">
                    Remember me
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
           
        </div>
    </div>
</div>
</body>
</html>