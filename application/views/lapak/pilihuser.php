<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="container">
		  <h1><?php echo $judul; ?></h1>
		  <h4><a href= "http://fjb.kaskus.co.id/product/<?php echo $link;?>">http://fjb.kaskus.co.id/product/<?php echo $link;?></a></h4>
		  <ul class="nav nav-tabs">
			<?php echo $wall; ?>
			<?php echo $compose; ?>
			<?php echo $management; ?>
		  </ul>
</div>

<div class="jumbotron">
		<?php if($error){ ?>
			<div class="container">
				<?php echo $error; ?>
			</div>
		<?php }?>
		<form method="post" id="kirimotomatis" action="<?php echo $action; ?>">
			<table class="table" border ="0">
			
			<tr>
				<td align="left" style="BORDER-RIGHT: #DDD 1px solid" width="300px">
				<h3>Pilih User untuk Sundul</h3>
				<div class="dropdown">
					<select id="sender">
					<option>-- Silahkan Pilih--</option>
					<?php foreach($pengirim as $pg){ ?>
						<option name = "pengirim" value="<?php echo $pg['user_id'];?>"><?php echo $pg['username'];?></option>
					<?php } ?>	
					</select>
				</div>
				<br/>
				Pesan Sundul otomatis diambil dari database.<br/>
				Berikut adalah pesan untuk sundul <? echo $judul; ?>.<br/>
				</td>
				<td align="left"><h3>Sundul Kaskus : </h3><textarea style="background:#FFF" name="message" id="message" class="form-control" cols="50" rows="10" placeholder = "" required readonly><?php echo $pesan;?></textarea></td>
				
			</tr>
			
			<tr>
				<br/>
				<td colspan="2" align="center"><button type="button" id="gowes" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Kirim </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			
			</table>
		</form>
</div>
<script>
	
	
	$('#sender').change(function(){
		var sender = $(this).val();
		if(sender == '2'){
			alert('Pengirim Rollman');
		}
		else if(sender == '3'){
			alert('Pengirim Setjo');
		}
		else if(sender == '4'){
			alert('Pengirim Wizemakers');
		}
		else if(sender == '5'){
			alert('Pengirim RIOT!!!');
		}
		else if(sender == '6'){
			alert('Pengirim ROCKABILLY');
		}
		$('#kirimotomatis').attr('action', "<?php echo $action;?>"+sender);
			
	});
	
	$('#gowes').click(function(){
		var pengirim = $('#pengirimotomatis').val();
		
		if(pengirim == ''){
			alert('Silahkan isi pengirim!');
			$('#kirimotomatis').focus();
		}
		else{
			$('#kirimotomatis').submit();
		}
	})
	
	$('#reset').click(function(){
		$('#linkthread').val("");
	});
</script>
		