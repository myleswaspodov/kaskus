<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="container">
		  <h1><?php echo $judul; ?></h1>
		  <h4><a href= "http://fjb.kaskus.co.id/product/<?php echo $link;?>">http://fjb.kaskus.co.id/product/<?php echo $link;?></a></h4>
		  <ul class="nav nav-tabs">
			<?php echo $wall; ?>
			<?php echo $compose; ?>
			<?php echo $management; ?>
		  </ul>
</div>
		
<div class="jumbotron">
		 <a class="btn btn-primary"  href="<?php echo $reset; ?>" onclick="return confirm('anda yakin akan mereset status?')"><i class='glyphicon glyphicon-repeat'></i> Reset Status</a>&nbsp;&nbsp;
		 
		<form method="post" action="<?php echo $simpan; ?>">
			<table class="table" border ="0">
			<tr>
				<td align="center">Tambah Sundulan : </td><td><textarea name="sundulan" id="sundulan" class="form-control" cols="50" rows="5" placeholder = "" required ></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Tambah </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
</div>
<div class="jumbotron">
		<h4>Sundulan Lapak <?php $keterangan;?></h4>
		<hr>
		<table class='table table-striped table-hover table-bordered table-responsive bordered' id='strip'>
		<thead style='background:#000;color:#fff'>
			<tr>
				<th style="background:black;color:white;" width="25">NO</th>
				<th style="background:black;color:white;" width="250">Isi</th>
				<th style="background:black;color:white;" width="50">status</th>
				<th style="background:black;color:white;" width="25">Action</th>
			</tr>
		</thead>
			<tbody>
				<?php $i=1; foreach($sundul as $sn){ ?>
				<tr>
					 <td><?php echo $i++; ?></td>	
					 <td><?php echo $sn['sundul_lapak_isi']; ?></td>	
					 <td><?php if($sn['sundul_lapak_status'] == 0){
						echo "Belum Tersundul";
					 }
					else{
						echo "Tersundul";
					}?></td>
					 
					 <td><a class="btn btn-primary" href="<?php echo $edit."/".$sn['id'] ?>"><i class='glyphicon glyphicon-pencil'></i></a> &nbsp;&nbsp;
						 <a class="btn btn-danger"  href="<?php echo $hapus."/".$sn['id'] ?>" onclick="return confirm('anda yakin akan hapus?')"><i class='glyphicon glyphicon-trash'></i></a></td>
				</tr>
				<?php } ?>
		
			</tbody>
		</table>
		
		</div>
		