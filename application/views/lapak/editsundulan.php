<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>

<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
<div class="container">
		  <h1><?php echo $judul; ?></h1>
		  <h4><a href= "http://fjb.kaskus.co.id/product/<?php echo $link;?>">http://fjb.kaskus.co.id/product/<?php echo $link;?></a></h4>
		  <ul class="nav nav-tabs">
			<?php echo $wall; ?>
			<?php echo $compose; ?>
			<?php echo $management; ?>
		  </ul>
</div>
		
<div class="jumbotron">
		
		<form method="post" action="<?php echo $edit;?>">
			<table class="table" border ="0">
			<tr>
				<?php foreach($sundulan as $sn){ ?>
				<td align="center">Edit Sundulan : </td><td><textarea name="sundulan" id="sundulan" class="form-control" cols="50" rows="5" placeholder = "" required ><?php echo $sn['sundul_lapak_isi'];?></textarea></td>
				<?php } ?>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Edit </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
</div>