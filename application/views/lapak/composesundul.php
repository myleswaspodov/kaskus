<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="container">
		  <h1><?php echo $judul; ?></h1>
		  <h4><a href= "http://fjb.kaskus.co.id/product/<?php echo $link;?>">http://fjb.kaskus.co.id/product/<?php echo $link;?></a></h4>
		  <ul class="nav nav-tabs">
			<?php echo $wall; ?>
			<?php echo $compose; ?>
			<?php echo $management; ?>
		  </ul>
</div>

<div class="jumbotron">
		<div>
		<a class="btn btn-primary" id="sundulmaut" href="<?php echo $maut?>">Sundul On Top</a>
		</div>
		<form method="post" id="sendcompose" action="<?php echo $action;?>">
			<table class="table" border ="0">
			<tr>
				<td align="left" style="BORDER-RIGHT: #DDD 1px solid" width="300px">
				
				
				<h3>Pilih User untuk Sundul</h3>
				<div class="dropdown">
					<select id="sender">
					<option>-- Silahkan Pilih--</option>
					<?php foreach($pengirim as $pg){ ?>
						<option name = "pengirim" value="<?php echo $pg['user_id'];?>"><?php echo $pg['username'];?></option>
					<?php } ?>	
					</select>
				</div>
				
				</td>
				<td><h3>Sundul Kaskus : </h3><textarea name="message" id="message" class="form-control" cols="50" rows="6" placeholder = "" required ></textarea></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><button type="submit" id="kirimpesan" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Kirim </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
		<hr/>
		<form method="post" id="sundulotomatis" action="<?php $sumon;?>">
			<p>Bingun komentar? klik ini : <button type="submit" class="btn btn-warning">Otomatis</button> </p>
			<textarea style="background:#FFF" name="pesan" id="pesan" class="form-control" cols="50" rows="2" placeholder = "" readonly><?php echo $pesan;?></textarea>
		</form>
			
</div>
<script>
$('#sender').change(function(){
		var sender = $(this).val();
		if(sender == '2'){
			alert('Pengirim Rollman');
		}
		else if(sender == '3'){
			alert('Pengirim Setjo');
		}
		else if(sender == '4'){
			alert('Pengirim Wizemakers');
		}
		else if(sender == '5'){
			alert('Pengirim RIOT!!!');
		}
		else if(sender == '6'){
			alert('Pengirim ROCKABILLY');
		}
		$('#sendcompose').attr('action', "<?php echo $action;?>"+sender);
		$('#sundulotomatis').attr('action', "<?php echo $sumon;?>"+sender);
				
	});
	
	$('#kirimpesan'). click(function(){
		var sender = $('#sender').val();
		var message = $('#message').val();
		
		if(sender == ''){
			alert("Silahkan isi pengirim dahulu!");
			$('#sender').focus();
		}
		else if(message == ''){
			alert("Silahkan isi sundulan!");
			$('#message').focus();
		}
		else{
			$('#sendcompose').submit();
		}
	})
</script>
		