<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Sundul Lapak Kaskus Cyberleech.com</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/bootstrap-3.3.4/css/style.css">
	<script type="text/javascript" src="<?php echo base_url()?>assets/jquery-11.2.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/bootstrap-3.3.4/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('#strip').dataTable( {
				 "bSort" : false
			} );
		} );
		
	
	</script>
</head>
<body>
	
	<nav class="navbar navbar-inverse navbar-fixed-top">
      		<div class="container">
        		<div class="container-fluid">
				<div class="navbar-header">
				  <a class="navbar-brand" href="#">Otomasi Kaskus</a>
				</div>
				<div>
				  <ul class="nav navbar-nav">
					<li><a href="<?php echo base_url();?>cek_pm">Pesan</a></li>
					<li class="active"><a href="<?php echo base_url();?>lapak">Lapak</a></li>
					<li><a href="<?php echo base_url();?>admin/user">Pengguna</a></li>
					<li><a href="<?php echo base_url();?>otomasi/logout">LogOut</a></li>
				  </ul>
				</div>
				</div>
			</div>
    </nav>
	
	<div class="container theme-showcase" role="main">
		<div class="container">
		  <center><a class="btn btn-primary" href="<?php echo base_url();?>lapak/tambahLapak">Tambah Lapak</a></center>
		</div>
		<ul class="pager">
		<?php $jumlah = count($thread)-1;?>
			<?php for($a=0; $a<=$jumlah; $a++){?>
			<li><a href="<?php echo base_url();?>lapak/fjb/<?php echo $thread[$a]['id'];?>"> Lapak <?php echo $thread[$a]['keterangan']; ?></a></li>
			<?php } ?>
		</ul>
		
	</div>

</body>
</html>