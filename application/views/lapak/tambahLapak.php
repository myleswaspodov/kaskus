<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
	
<div class="jumbotron">
		<form method="post" action="<?php echo $simpan; ?>">
			<table class="table" border ="0">
			<tr>
				<td align="center">Thread : </td><td><textarea name="threadnew" id="threadnew" class="form-control" cols="50" rows="1" placeholder = "" required ></textarea></td>
			</tr>
			<tr>
				<td align="center">Pemilik Lapak : </td><td>
					<select id="pemilik lapak" name = "pemilik">
						<option>-- Silahkan Pilih--</option>
						<?php foreach($pengirim as $pg){ ?>
							<option value="<?php echo $pg['user_id'];?>"><?php echo $pg['username'];?></option>
						<?php } ?>
					</select></td>
			</tr>
			<tr>
				<td align="center">Keterangan : </td><td><textarea name="keterangan" id="keterangan" class="form-control" cols="50" rows="3" placeholder = "" required ></textarea></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Tambah </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
</div>