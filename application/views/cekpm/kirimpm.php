<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="jumbotron">
		<h4>Balas Pesan</h4>
		
		<hr>
		<table class="table table-striped table-hover table-bordered table-responsive bordered strip" data-page-length="10">
		<thead style='background:#000;color:#fff'>
			<tr>
				<th style="background:black;color:white;" width="100">Tanggal</th>
				<th style="background:black;color:white;" width="75">User Kaskus</th>
				<th style="background:black;color:white;" width="200">Subject</th>
				<th style="background:black;color:white;">Isi</th>
				
			</tr>
		</thead>
			<tbody>
				<?php foreach($sundul as $sn){ ?>
				<tr>
					<td><?php echo mdate("%d %M %Y <BR/>\n%H:%i:%s", strtotime($sn['tanggal'])); ?></td>	
					<td><?php echo "<a href='http://www.kaskus.co.id/profile/".$sn['user_id']."'>".$sn['username']."</a>"; ?></td>
					<td><?php echo $sn['subject']; ?></td>
					<td><?php echo $sn['isi']; ?></td> 		 
				</tr>
				<?php } ?>
		
			</tbody>
		</table>	
</div>
		
<div class="jumbotron">
		<form method="post" action="<?php echo base_url() ?>cek_pm/send/<?php echo $idrepl."/".$pengirim; ?>">
			<table class="table" border ="0">
			<tr>
				<td align="center">Pesan Untuk Kaskus</td><td><textarea name="message" id="message" class="form-control" cols="50" rows="10" placeholder = "" required ><?php echo "[QUOTE=".$sn['username']."]"; ?><?php echo $sn['isi']."[/QUOTE]"; ?></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button type="submit" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Kirim </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
</div>		