<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>
		
		
		<div class="jumbotron">
		<h4>Daftar Kotak Masuk</h4>
		
		<hr>
		<table class="table table-striped table-hover table-bordered table-responsive bordered strip" data-page-length="10">
		<thead style='background:#000;color:#fff'>
			<tr>
				<th style="background:black;color:white;" width="100">Tanggal</th>
				<th style="background:black;color:white;" width="75">Pengirim</th>
				<th style="background:black;color:white;" width="200">Subject</th>
				<th style="background:black;color:white;">Isi</th>
				<th style="background:black;color:white;">Aksi</th>
			</tr>
		</thead>
			<tbody>
				<?php foreach($sundul as $sn){ ?>
				<tr>
					<td><?php echo mdate("%d %M %Y <BR/>\n%H:%i:%s", strtotime($sn['tanggal'])); ?></td>
					<td><?php echo "<a href='http://www.kaskus.co.id/profile/".$sn['user_id']."'>".$sn['username']."</a>"; ?></td>
					<td><?php echo $sn['subject']; ?></td>
					<td><?php echo $sn['isi']; ?></td>
					
					<td><?php if($sn['stat'] == 0){ ?>
					 	 <a class="btn btn-primary" href="<?php echo base_url() ?>cek_pm/balas/<?php echo $sn['id_pmasli']."/".$iduser;?>" target="_blank">Balas</a>
					 	 <a class="btn btn-danger"  href="<?php echo base_url() ?>cek_pm/sudahdibaca/<?php echo $sn['id_pm']."/".$iduser;?>" onclick="return confirm('Tandai pesan sudah dibaca?')">Tandai</a>&nbsp;&nbsp;
					 <?php } else{ ?>
					 	<a class="btn btn-primary">Sudah dibaca</a>
					 <?php } ?> 
					 </td>
									 
				</tr>
				<?php } ?>
		
			</tbody>
		</table>
		
		</div>