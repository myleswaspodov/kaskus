<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="container">
		  <h1>Otomasi Pesan Masuk</h1>
		  <ul class="nav nav-tabs">
			<li><a href="<?php echo base_url();?>cek_pm/compose">Tulis Pesan</a></li>
			<li class="active"><a href="<?php echo base_url();?>cek_pm">Pesan Masuk</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/dibaca">Sudah di Baca</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/dihapus">Pesan diHapus</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/outbox/">Pesan Keluar</a></li>
		  </ul>
</div>
		<?php $total = count($user)-1;
		for($x=0; $x<=$total; $x++){
		?>
		<div class="jumbotron">
			<h4>Pesan <?php echo $user[$x]['username'];?></h4>
			
			<table class="table table-striped table-hover table-bordered table-responsive bordered strip" data-page-length="10">
		<thead style="background:#000;color:#fff">
			<tr>
				<th style="background:black;color:white;" width="100">Tanggal</th>
				<th style="background:black;color:white;" width="75">Pengirim</th>
				<th style="background:black;color:white;" width="200">Subject</th>
				<th style="background:black;color:white;">Isi</th>
				<th style="background:black;color:white;" width="50">Aksi</th>
			</tr>
		</thead>
			<tbody>
				<?php foreach($pesan as $se){ 
				if($se['penerima'] == $user[$x]['user_id']){
				?>
				<tr>
					<td><?php echo mdate("%d %M %Y <BR/>\n%H:%i:%s", strtotime($se['tanggal'])); ?></td>
					<td><?php echo "<a href='http://www.kaskus.co.id/profile/".$se['user_id']."'>".$se['username']."</a>"; ?></td>
					<td><?php echo $se['subject']; ?></td>
					<td><?php echo $se['isi']; ?></td>
					
					 <td><?php if($se['stat'] == 0){ ?>
						<a class="btn btn-primary" href="<?php echo base_url() ?>cek_pm/balas/<?php echo $se['id_pmasli']."/".$se['penerima'];?>" target="_blank"><span class="glyphicon glyphicon-send"></span></a>
						&nbsp;&nbsp;
						<a class="btn btn-warning"  href="<?php echo base_url() ?>cek_pm/sudahdibaca/<?php echo $se['id_pm'];?>" onclick="return confirm('Tandai pesan sudah dibaca?')"><span class="glyphicon glyphicon-ok"></span></a>
					 	&nbsp;&nbsp;
						<a class="btn btn-danger"  href="<?php echo base_url() ?>cek_pm/delete/<?php echo $se['id_pm']."/".$se['id_pmasli']."/3";?>" onclick="return confirm('Anda yakin pesan akan dihapus?')"><span class="glyphicon glyphicon-trash"></span></a>
					  <?php }?>
					 </td>
									 
				</tr>
				<?php }} ?>
		
			</tbody>
		</table>
		</div>
		<?php }?>