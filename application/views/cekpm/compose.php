<?php 
	if($this->session->flashdata('error')){
	$error = $this->session->flashdata('error');
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error</strong> <?php echo $error ?>
</div>
<?php
}
else if($this->session->flashdata('success')){
$success = $this->session->flashdata('success');
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success</strong> <?php echo $success ?>
</div>

<?php
} 
?>

<div class="container">
		  <h1>Otomasi Pesan Masuk</h1>
		  <ul class="nav nav-tabs">
			<li  class="active"><a href="<?php echo base_url();?>cek_pm/compose">Tulis Pesan</a></li>
			<li><a href="<?php echo base_url();?>cek_pm">Pesan Masuk</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/dibaca">Sudah di Baca</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/dihapus">Pesan diHapus</a></li>
			<li><a href="<?php echo base_url();?>cek_pm/outbox/">Pesan Keluar</a></li>
		  </ul>
</div>

<div class="jumbotron">
	<div class="container">
	  <p>Pilih Pengirim : </p>                                          
	  <div class="dropdown">
		<select id="sender">
		<option>-- Silahkan Pilih--</option>
			<?php foreach($pengirim as $pg){ ?>
					<option name = "pengirim" value="<?php echo $pg['user_id'];?>"><?php echo $pg['username'];?></option>
			<?php } ?>	
		</select>
	  </div>
	</div>
</div>
		
<div class="jumbotron">
		<form method="post" id="sendcompose" action="<?php echo base_url() ?>cek_pm/sendcompose/>
			<table class="table" border ="0">
			<tr>
				<td align="left">To</td><td><input type="text" name="to" id="to" class="form-control" placeholder = "" required ></td>
			</tr>
			<tr>
				<td align="left">Subject</td><td><input type="text" name="subject" id="subject" class="form-control" placeholder = "" required ></td>
			</tr>
			<tr>
				<td align="left">Pesan Untuk Kaskus</td><td><textarea name="message" id="message" class="form-control" cols="50" rows="10" placeholder = "" required ></textarea></td>
			</tr>
			<tr>
				<br/><br/>
				<td colspan="2" align="center"><button type="button" id="kirimpesan" name="simpan" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Kirim </button>&nbsp;&nbsp;
				<button type="button" id="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i> Reset</button></td>
			</tr>
			</table>
		</form>
</div>

		