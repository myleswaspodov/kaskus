<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	//require_once APPPATH.'/controllers/Sundul.php';
	
	Class Admin extends CI_Controller {
		
		function __construct(){
			parent::__construct();
			$this->load->model('admin_model');
			
			if (($this->session->userdata('login') == '') || ($this->session->userdata('login') != 'budijake'))
			{
				redirect('otomasi');
			}
		}
		
		function thread(){
			$data['judul'] = 'thread';
			$data['thread'] = $this->admin_model->getThreadAll();
			$data['isi'] = 'admin/thread';
			$this->load->view('admin/templateAdmin', $data);
		}
		
		function simpanThread(){
			$link = $this->input->post('linkthread').'/';
			if(preg_match('#\b([\w-]+://?|www[.]|fjb[.])kaskus\.co\.id\/(.+?)\/(.+?)\/#iS', $link, $match)){
			
				if(stristr($match[1], 'http')){
					$linkthread = 'http://www.kaskus.co.id/'.$match[2].'/'.$match[3] .'/';
				}
				else{
					$linkthread = 'http://'.$match[1].'kaskus.co.id/'.$match[2].'/'.$match[3] .'/';
				}
				
				$this->admin_model->simpanThread($linkthread);
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
				redirect('admin/thread');
			}
			else{
				$this->session->set_flashdata('error', 'Format link salah');
				redirect('admin/thread');
			}
		}
		
		function deleteThread($id_thread){
			$this->admin_model->deleteThread($id_thread);
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('admin/thread');
		}
		function editThread($id_thread){
			$data['judul'] = 'edit thread';
			$data['thread'] = $this->admin_model->getThreadId($id_thread);
			$data['isi'] = 'admin/edit_thread';
			$this->load->view('admin/templateAdmin', $data);
		}
		
		function sundulCmf(){
			$data['judul'] = 'Sundul template';
			$data['sundul'] = $this->admin_model->getSundulTemp();
			$data['isi'] = 'admin/sundul_cmf';
			$this->load->view('admin/templateAdmin', $data);
		}
		function simpanSundulCmf(){
			$this->admin_model->simpanSundulCmf();
			$this->session->set_flashdata('success', 'Data berhasil disimpan');
			redirect('admin/sundulCmf');
		}
		
		function editSundulCmf($id_sundul){
			$data['judul'] = 'edit sundul cmf';
			$data['sundul'] = $this->admin_model->getSundulcmfId($id_sundul);
			$data['isi'] = 'admin/edit_sundulcmf';
			$this->load->view('admin/templateAdmin', $data);
		}
		function deleteSundulCmf($id_sundul){
			$this->admin_model->deleteSundulCmf($id_sundul);
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('admin/sundulCmf');
		}
		//------------------------------------------------------------------------------------------------------------
		function user(){
			$data['judul'] = 'user';
			$data['user'] = $this->admin_model->getUser();
			$data['isi'] = 'admin/user';
			$this->load->view('admin/khususadmin', $data);
		}
		
		function editUser($id){
			$data['judul'] = 'user';
			$data['user'] = $this->admin_model->getUserId($id);
			$data['isi'] = 'admin/edit_user';
			$this->load->view('admin/khususadmin', $data);
		}
		
		function login($username, $pass){
		
			$loginURL = 'https://www.kaskus.co.id/user/login/';
			
			$page = $this->functions->curl($loginURL);
					
			$post = array();
			$post['securitytoken'] = $this->functions->cut_str($page, 'name="securitytoken" value="', '"');
			$post['url'] = '/user/login/';
			$post['md5password'] = md5($pass);
			$post['md5password_utf'] = md5($pass);
			$post['username'] = $username;
			$post['password'] = '';
			$post['rememberme'] = 'rememberme';
			
			
			$page = $this->functions->curl($loginURL, 0, $post, $loginURL);
			$cookie = $this->functions->GetCookies($page) . "; ";
			
			$page = $this->functions->curl('https://www.kaskus.co.id/');
			
			return $cookie;
		}
		
		function simpanUser(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			$cookie = $this->login($username, $password);
			
			$page = $this->functions->curl('http://www.kaskus.co.id/', $cookie);
			
			//$this->functions->debug($page, $cookie);
			
			if(stristr($page, 'Sign in | Join')){
				$this->session->set_flashdata('error', 'username dan password salah');
				redirect('admin/user');
			}		
			else{
				$this->admin_model->simpanUser($cookie);
				$this->session->set_flashdata('success', 'Data berhasil disimpan');
				redirect('admin/user');
			}
		}
		
		function deleteUser($id_user){
			$this->admin_model->deleteUser($id_user);
			$this->session->set_flashdata('success', 'Data berhasil dihapus');
			redirect('admin/user');
		}
	}
?>