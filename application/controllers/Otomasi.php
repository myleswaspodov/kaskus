<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Otomasi extends CI_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->library('email');
	}
	
	function index(){
		$data['error'] = '';
		$this->load->view('loginOtomasi', $data);
	}
	
	function login(){
		$username = $this->input->post('username');
		$pass = $this->input->post('pass');
		
		$u = "kaskus";
		$p = "otomasisundul";
		
		if(($u == $username) && ($p == $pass)){
			$this->session->set_userdata('login', 'budijake');
			redirect('cek_pm');
		}
		else{
			$data['error'] = "username dan password salah";
			$this->load->view('loginOtomasi', $data);
		}
	}
	
	public function logout()
    {
        $this->session->unset_userdata(array('login' => '',));
        $this->session->sess_destroy();
		redirect('otomasi');
    }
}