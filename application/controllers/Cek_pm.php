<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_pm extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('M_cek_pm', 'pm', TRUE);
		//$this->load->model('Kaskus_model');
		$this->load->model('M_kaskus_jokes', 'jokes', TRUE);
		$this->load->model('M_outbox', 'out', TRUE);
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->library('email');
		
		if (($this->session->userdata('login') == '') || ($this->session->userdata('login') != 'budijake'))
        {
            redirect('otomasi');
        }
	}
	
	function index(){
		$data['isi'] = 'cekpm/semuauser';
		$data['user'] = $this->pm->cek_user();
		$data['pesan'] = $this->pm->pesan();
		$this->load->view('cekpm/cek_pm', $data);
	}
	
	function lihat(){
		$data = $this->pm->cek_user();
		print_r($data);
	}
	
	function dibaca(){
		$data['isi'] = 'cekpm/semuadibaca';
		$data['user'] = $this->pm->cek_user();
		$data['pesan'] = $this->pm->pesandibaca();
		$this->load->view('cekpm/cek_pm', $data);
	}
	
	function dihapus(){
		$data['isi'] = 'cekpm/semuadihapus';
		$data['user'] = $this->pm->cek_user();
		$data['pesan'] = $this->pm->pesandihapus();
		$this->load->view('cekpm/cek_pm', $data);
	}
	
	function outbox(){
		$data['isi'] = 'cekpm/outbox';
		$data['user'] = $this->pm->cek_user();
		$data['pesan'] = $this->out->outbox();
		$this->load->view('cekpm/cek_pm', $data);
	}
	
	function login($username, $pass){
		
		$loginURL = 'https://www.kaskus.co.id/user/login/';
		
		$page = $this->functions->curl($loginURL);
	
		$post = array();
		$post['securitytoken'] = $this->functions->cut_str($page, 'name="securitytoken" value="', '"');
		$post['url'] = '/user/login/';
		$post['md5password'] = md5($pass);
		$post['md5password_utf'] = md5($pass);
		$post['username'] = $username;
		$post['password'] = '';
		$post['rememberme'] = 'rememberme';
		
		
		$page = $this->functions->curl($loginURL, 0, $post, $loginURL);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		$page = $this->functions->curl('https://www.kaskus.co.id/');
		
		$simpancook = $this->jokes->saveCookie($username, $cookie);
		return $cookie;
		$this->session->set_userdata('cookie', $cookie);
	}
	
	public function pesan_masuk($id){
		$link = "www.kaskus.co.id/pm/index/";
		//ambil user
		$user = $this->jokes->getUser($id);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		//getpage
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//$this->functions->debug($page);
		
		//jika ada pesan baru
		if(stristr($page, "emailnew.png")){
			$patt = '/<img src=\"http:\/\/i.kaskus.id\/e3.1\/img\/icon\/emailnew.png\"> <\/td> <td> (.+?) <\/td> <td> <a href=\"(.+?)\"> <span class=\"title\" style=\"\"> (.+?) <\/span> <\/a> <\/td> <td> (.+?) <\/td>/';
			preg_match_all($patt, $page, $hasil);
			//print_r($hasil);
		
		
		
			$total = count($hasil[1])-1;
			//echo $total;
		
			for($a=0; $a<=$total; $a++){
				$user = $hasil[1][$a];
				$alamat_pm = "http://www.kaskus.co.id".$hasil[2][$a];
				
				$untuk_pm = '/view\/0\/(.+)/';
				preg_match($untuk_pm, $hasil[2][$a], $idpmasli);
				$idnyapmasli = $idpmasli[1];
				$subject = $hasil[3][$a];
				
				
				//masuk ke alamat PM
				$page = $this->functions->curl($alamat_pm, $cookie);
				$cookie = $this->functions->GetCookies($page) . "; ";
				
				//$this->functions->debug($page);
				
				$user_id = $this->functions->cut_str($page, 'Sender</span><a href="/profile/', '"');
				$isi = $this->functions->cut_str($page, '<textarea id="message" name="message" rows="10" cols="60" tabindex="1" dir="ltr"  onfocus="countChars()" onkeydown="countChars()" onkeyup="countChars()">', '</textarea>');
				
				//tanggal
				$tgl = $this->functions->cut_str($page, '<span>Date</span>', '</div>');
				$tanggal= date('Y-m-d H:i:s', strtotime($tgl));
				
				
				//tanggal untuk email
				$date = $this->functions->cut_str($page, 'Date</span>', '</div>');
				
				//savedatabase
				$this->pm->savepesan($user, $user_id, $subject, $isi, $tanggal, $idnyapmasli, $id);
				
				//cekpenerima
				if($id == 2){
					$penerima = "Rollman";
				}
				elseif($id == 3){
					$penerima = "Setjo";
				}
				else{
					$penerima = 'Setan';
				}
				
				//kirim email
				$mesej = "Pesan Baru\nPENERIMA : ".$penerima."\n\nPENGIRIM : ".$user."\n\nUSER ID : ".$user_id."\n\nTanggal Pengiriman : ".$date."\n\nAlamat PM : ".$alamat_pm."\n\nSubject : ".$subject."\n\nPesan : ".$isi;
				$this->sendEmail($mesej);
				
			}
			
			echo "Ada pesan baru untuk Anda, silahkan cek email Anda atau click <a href='http://cloudmyfile.com/kaskus/cek_pm'>link ini</a>";			
		}
		else{
			echo "Tidak ada pesan baru untuk Anda, bos!";
		}
	}
	
	public function sudahdibaca($id){
		$this->pm->updateStatusLink($id);
		redirect('cek_pm');
	}
	
	public function balas($id, $pengirim){
		$data['sundul'] = $this->pm->detail($id);
		$data['idrepl'] = $id;
		$data['pengirim'] = $pengirim;
		$data['judul'] = 'Kirim Mesej';
		$data['isi'] = 'cekpm/kirimpm';
		$this->load->view('cekpm/cek_pm', $data);
	}
	
	public function send($id, $pengirim){
		
		$user = $this->jokes->getUser($pengirim);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "www.kaskus.co.id/pm/view/0/".$id;
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		$recip = $this->functions->cut_str($page, '<input type="hidden" name="recipient" value="&quot;', '&quot;');
		
		$send = "http://www.kaskus.co.id/pm/reply/".$id;
		$page = $this->functions->curl($send, $cookie, 0, $link);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		$subject = $this->functions->cut_str($page, '<input id="xlInput" value="', '"');
		
		$post = array();
		$post['recipient'] =  '"'.$recip.'"';
		$post['subject'] = $subject;
		$post['message'] = $this->input->post('message');
		$post['securitytoken'] = $token;
		$post['send'] = 'Send Message';
		
		$page = $this->functions->curl('http://www.kaskus.co.id/pm/compose', $cookie, $post, $send);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		redirect('cek_pm');
		
		
	}
	
	public function sendEmail($mesej){   
		  $config = Array(
		     'protocol' => 'smtp',
		     'smtp_host' => 'support@cloudmyfile.com',
		     'smtp_port' => 465,
		     'smtp_user' => 'support@cloudmyfile.com',
		     'smtp_pass' => 'khususuntuksupport', 
		     'mailtype' => 'html',
		     'charset' => 'iso-8859-1',
		     'wordwrap' => TRUE
		  ); 
		 
		  $this->load->library('email', $config);
		  $this->email->from('support@cloudmyfile.com', "Budi Jake");
		  //$this->email->to("error@cyberleech.com");
		  $this->email->to("guntur_heretic00@yahoo.com");
		  $this->email->cc("wizemakers@gmail.com");
		  $this->email->bcc("support@cloudmyfile.com");
		  $this->email->subject("Cloudmyfile Info - PM Kaskus");
		  $this->email->message($mesej);
		  
		  $this->email->send();
	 }
	 
	public function delete($idpesan, $idkas, $pengirim){
	 	$user = $this->jokes->getUser($pengirim);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "http://www.kaskus.co.id/pm/delete/0/".$idkas;
		$ref = "http://www.kaskus.co.id/pm/view/0/".$idkas;
		
		$page = $this->functions->curl($link, $cookie, 0, $ref);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie, 0, $ref);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		$this->pm->updateHapus($idpesan);
		redirect('cek_pm');
		
	}
	 
	public function compose(){
		$data['pengirim'] = $this->jokes->pengguna();;
		$data['judul'] = 'Tulis pesan';
		$data['isi'] = 'cekpm/compose';
		$this->load->view('cekpm/cek_pm', $data);
	}

	public function sendcompose($pengirim){
		$user = $this->jokes->getUser($pengirim);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "http://www.kaskus.co.id/pm/compose";
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		
		//post
		$post = array();
		$post['recipient'] = '"'.$this->input->post('to').'"';
		$post['subject'] = $this->input->post('subject');
		$post['message'] = $this->input->post('message');
		$post['securitytoken'] = $token;
		$post['send'] = "Send Message";
		
		//kirim pesan
		$page = $this->functions->curl($link, $cookie, $post, $link);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		redirect('cek_pm');
	
	}
}