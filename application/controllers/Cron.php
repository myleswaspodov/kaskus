<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('M_kaskus_jokes', 'jokes', TRUE);
		$this->load->model('M_cek_reply', 'reply', TRUE);
		$this->load->model('M_lapak', 'lapak', TRUE);
		$this->load->model('Mlapak', 'jual', TRUE);
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->library('email');
	
	}
	
	function cek_semua(){
		$this->outbox(2);
		$this->pesan_masuk(2);
		$this->outbox(3);
		$this->pesan_masuk(3);
		$this->cek_thread(3);
		$this->cek_fjb(3);
		$this->outbox(5);
	}
	
	function login($username, $pass){
		
		$loginURL = 'https://www.kaskus.co.id/user/login/';
		
		$page = $this->functions->curl($loginURL);
	
		$post = array();
		$post['securitytoken'] = $this->functions->cut_str($page, 'name="securitytoken" value="', '"');
		$post['url'] = '/user/login/';
		$post['md5password'] = md5($pass);
		$post['md5password_utf'] = md5($pass);
		$post['username'] = $username;
		$post['password'] = '';
		$post['rememberme'] = 'rememberme';
		
		
		$page = $this->functions->curl($loginURL, 0, $post, $loginURL);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		$page = $this->functions->curl('https://www.kaskus.co.id/');
		
		$simpancook = $this->jokes->saveCookie($username, $cookie);
		return $cookie;
		$this->session->set_userdata('cookie', $cookie);
	}
	
	public function pesan_masuk($id){
		$link = "www.kaskus.co.id/pm/index/";
		//ambil user
		$user = $this->jokes->getUser($id);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		//getpage
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//$this->functions->debug($page);
		
		//jika ada pesan baru
		if(stristr($page, "emailnew.png")){
			$patt = '/<img src=\"http:\/\/i.kaskus.id\/e3.1\/img\/icon\/emailnew.png\"> <\/td> <td> (.+?) <\/td> <td> <a href=\"(.+?)\"> <span class=\"title\" style=\"\"> (.+?) <\/span> <\/a> <\/td> <td> (.+?) <\/td>/';
			preg_match_all($patt, $page, $hasil);
			//print_r($hasil);
		
		
		
			$total = count($hasil[1])-1;
			//echo $total;
		
			for($a=0; $a<=$total; $a++){
				$user = $hasil[1][$a];
				$alamat_pm = "http://www.kaskus.co.id".$hasil[2][$a];
				
				$untuk_pm = '/view\/0\/(.+)/';
				preg_match($untuk_pm, $hasil[2][$a], $idpmasli);
				$idnyapmasli = $idpmasli[1];
				$subject = $hasil[3][$a];
				
				
				//masuk ke alamat PM
				$page = $this->functions->curl($alamat_pm, $cookie);
				$cookie = $this->functions->GetCookies($page) . "; ";
				
				//$this->functions->debug($page);
				
				$user_id = $this->functions->cut_str($page, 'Sender</span><a href="/profile/', '"');
				$isi = $this->functions->cut_str($page, '<textarea id="message" name="message" rows="10" cols="60" tabindex="1" dir="ltr"  onfocus="countChars()" onkeydown="countChars()" onkeyup="countChars()">', '</textarea>');
				
				//tanggal
				$tgl = $this->functions->cut_str($page, '<span>Date</span>', '</div>');
				$tanggal= date('Y-m-d H:i:s', strtotime($tgl));
				
				//isi
				$isipesan = $this->functions->cut_str($page, '<div class="message-body">', '</div>');
				
				//tanggal untuk email
				$date = $this->functions->cut_str($page, 'Date</span>', '</div>');
				
				//savedatabase
				$this->pm->savepesan($user, $user_id, $subject, $isi, $tanggal, $idnyapmasli, $id, $isipesan);
				
				//cekpenerima
				if($id == 2){
					$penerima = "Rollman";
				}
				elseif($id == 3){
					$penerima = "Setjo";
				}
				else{
					$penerima = 'Setan';
				}
				
				//kirim email
				$mesej = "Pesan Baru\nPENERIMA : ".$penerima."\n\nPENGIRIM : ".$user."\n\nUSER ID : ".$user_id."\n\nTanggal Pengiriman : ".$date."\n\nAlamat PM : ".$alamat_pm."\n\nSubject : ".$subject."\n\nPesan : ".$isi;
				$this->sendEmail($mesej);
				
			}
			
			echo "Ada pesan baru untuk Anda, silahkan cek email Anda atau click <a href='http://cloudmyfile.com/kaskus/cek_pm'>link ini</a>";			
		}
		else{
			echo "Tidak ada pesan baru untuk Anda, bos!";
		}
	}
	
	
	function cek_lapak($id){
		$th = $this->jual->threadKaskus($id);
		$idLapak = $th[0]['id'];
		$users = $this->jual->allUser();
		$jumlahUser = count($users)-1;
		
		$thread = $th[0]['thread'];
		
		$link = "http://fjb.kaskus.co.id/product/".$thread;
		
		//kalo thread lebih dari 1 halaman
		if(stristr($page, '<li class="page-count">')){
			
			//cek banyak page
			$totalPage = $this->functions->cut_str($page, '<li class="page-count">Page 1 of ', '</li>');
			
			//cek flag kalo flag kosong
			$cek_flag = $this->reply->flag_fjb();
			
			//ambil link untuk page
			$cute = $this->functions->cut_str($page, '<li class="active"><a>1</a></li><li><a href="', '">');
			$patt = '/\/(.+?)\/(.+?)\/(.+?)\/(.+)/';
			preg_match_all($patt, $cute, $linknya);
			
			$linknext = "http://fjb.kaskus.co.id/".$linknya[1][0]."/".$linknya[2][0]."/".$linknya[3][0];
				
			
			if(is_null($cek_flag[0]['MAX(flag)'])|| empty($cek_flag[0]['MAX(flag)'])){
				for($a=1; $a<= $totalPage; $a++){
					$linkpage = $linknext."/".$a;
					echo "<hr>";
					echo $linkpage;
					
					$page = $this->functions->curl($linkpage, $cookie);
					//$this->functions->debug($page);
				
					//posid
					$postid = '/<div class=\"permalink\"><a href=\"\/show_post\/(.+?)\/(.+?)\/(.+?)\" target/';
					preg_match_all($postid, $page, $aidipos);
					//print_r($aidipos);
					$post = $aidipos[1];
					
					$jumlah_post = count($post)-1;
					
					for($b=0; $b<=$jumlah_post; $b++){
						$linkpost = "http://fjb.kaskus.co.id/show_post/".$aidipos[1][$b];
						
						//idpos
						$postid = $aidipos[1][$b];
						
						$page = $this->functions->curl($linkpost, $cookie);
						$cookie = $this->functions->GetCookies($page) . "; ";
						
						//$this->functions->debug($page);
						
						//ambilisi
						$isi = $this->functions->cut_str($page, '<div class="entry" itemprop="text">', '<footer class="entry-footer clearfix">');
						
						//ambiltgl
						$tanggal = $this->functions->cut_str($page, '<time class="entry-date" datetime="', '+');
						$tgl = str_replace('T', ' ', $tanggal);
						
						//userid
						$userid = $this->functions->cut_str($page, '<div class="user-name" data-userid="', '"');
						
						//user
						$user = $this->functions->cut_str($page, 'class="nickname" itemprop="name"> ', ' </a>');
						
						echo "<br/>";
						echo $linkpost;
						echo "<br/>";
						echo $tgl;
						echo "<br/>";
						echo $postid;
						echo "<br/>";
						echo $isi;
						echo "<br/>";
						echo $userid;
						echo "<br/>---------------------------------------------";
						
						for($us = 0; $us <= $jumlahUser; $us++){
							if(stristr($userid,$users[$us]['idprofil']){
								echo "Pengirim merupakan komplotan kita";
							}
							else{
								$this->lapak->savethread('$idLapak, $user, $userid, $tgl, $isi, $postid, $totalPage);
								$message= "Balasan Thread Lapak\n\n\nTanggal Pengiriman : ".$tgl."\n\nPENGIRIM : ".$user."\n\nUSER ID : ".$userid."\n\nPost ID : ".$postid."\n\nPesan : ".$isi;
								$this->sendEmail($message);
							}
						}
					}
				}
			
			}
			else{
				for($a=$cek_flag[0]['MAX(flag)']; $a<= $totalPage; $a++){
					$linkpage = $linknext."/".$a;
					$page = $this->functions->curl($linkpage, $cookie);
					//$this->functions->debug($page);
					
					//posid
					$postid = '/<div class=\"permalink\"><a href=\"\/show_post\/(.+?)\/(.+?)\/(.+?)\" target/';
					preg_match_all($postid, $page, $aidipos);
					print_r($aidipos);
					$post = $aidipos[1];
					
					$jumlah_post = count($post)-1;
					echo  $jumlah_post;
					for($b=0; $b<=$jumlah_post; $b++){
						$linkpost = "http://fjb.kaskus.co.id/show_post/".$aidipos[1][$b];
						
						//idpos
						$postid = $aidipos[1][$b];
						
						$page = $this->functions->curl($linkpost, $cookie);
						$cookie = $this->functions->GetCookies($page) . "; ";
						
						//ambilisi
						$isi = $this->functions->cut_str($page, '<div class="entry" itemprop="text">', '<footer class="entry-footer clearfix">');
						
						//ambiltgl
						$tanggal = $this->functions->cut_str($page, '<time class="entry-date" datetime="', '+');
						$tgl = str_replace('T', ' ', $tanggal);
						
						//userid
						$userid = $this->functions->cut_str($page, '<div class="user-name" data-userid="', '"');
						
						//user
						$user = $this->functions->cut_str($page, 'class="nickname" itemprop="name"> ', ' </a>');
						
						
						for($us = 0; $us <= $jumlahUser; $us++){
							if(stristr($userid,$users[$us]['idprofil']){
								echo "Pengirim merupakan komplotan kita";
							}
							else{
								$this->jual->savethread($idLapak, $user, $userid, $tgl, $isi, $postid, $totalPage);
								$mix = $this->jual->saringid($postid);
								
								
								if(is_null($mix) || empty($mix)){
									$message= "Balasan Thread Lapak\n\n\nTanggal Pengiriman : ".$tgl."\n\nPENGIRIM : ".$user."\n\nUSER ID : ".$userid."\n\nPost ID : ".$postid."\n\nPesan : ".$isi;
									$this->sendEmail($message);
								}
								else{
									echo "sudah ada tercatat, tidak ada yang baru";
								}
							}
						}
												
					}
				}
			}
		}
		else{
						
			//posid
			$postid = '/<div class=\"permalink\"><a href=\"\/show_post\/(.+?)\/(.+?)\/(.+?)\" target/';
			preg_match_all($postid, $page, $aidipos);
			print_r($aidipos);
			$post = $aidipos[1];
				
			$jumlah_post = count($post)-1;
			echo  $jumlah_post;
			for($b=0; $b<=$jumlah_post; $b++){
				$linkpost = "http://fjb.kaskus.co.id/show_post/".$aidipos[1][$b];
						
				//idpos
				$postid = $aidipos[1][$b];
						
				$page = $this->functions->curl($linkpost, $cookie);
				$cookie = $this->functions->GetCookies($page) . "; ";
				$this->functions->debug($link);
						
				$this->functions->debug($page);
						
				//ambilisi
				$isi = $this->functions->cut_str($page, '<div class="entry" itemprop="text">', '<footer class="entry-footer clearfix">');
						
				//ambiltgl
				$tanggal = $this->functions->cut_str($page, '<time class="entry-date" datetime="', '+');
				$tgl = str_replace('T', ' ', $tanggal);
							
				//userid
				$userid = $this->functions->cut_str($page, '<div class="user-name" data-userid="', '"');
						
				//user
				$user = $this->functions->cut_str($page, 'class="nickname" itemprop="name"> ', ' </a>');
						
					
						
				for($us = 0; $us <= $jumlahUser; $us++){
					if(stristr($userid,$users[$us]['idprofil']){
						echo "Pengirim merupakan komplotan kita";
					}
					else{
						$this->jual->savethread($idLapak, $user, $userid, $tgl, $isi, $postid, $totalPage);
						$mix = $this->jual->saringid($postid);
						
						
						if(is_null($mix) || empty($mix)){
							$message= "Balasan Thread Lapak\n\n\nTanggal Pengiriman : ".$tgl."\n\nPENGIRIM : ".$user."\n\nUSER ID : ".$userid."\n\nPost ID : ".$postid."\n\nPesan : ".$isi;
							$this->sendEmail($message);
						}
						else{
							echo "sudah ada tercatat, tidak ada yang baru";
						}
					}
				}
					
			}
		}	
	}
	
	/* ----------------------------------- Outbox--------------------------------------------- */
	
	
	public function outbox($id){
		
		$link = "http://www.kaskus.co.id/pm/index/-1";
		//ambil user
		$user = $this->jokes->getUser($id);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		//getpage
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//liat udah berapa
		$totalmessage = $this->functions->cut_str($page, 'You have <b>', '</b>');
		echo "Kapasitas penyimpanan pesan = ".$totalmessage;
		
		//kalo ada banyak halaman
		if(stristr($page, '<li class="page-count">')){
			//ambil jumlah halaman
			$jumlah_hal = $this->functions->cut_str($page, '<li class="page-count">Page 1 of ', '</li>');
			
			//cekdatabase nyampe halaman brp
			$cek_flag = $this->out->flag($id);
			
			//kalo belum pernah ngegrab
			if(is_null($cek_flag[0]['MAX(flag)'])|| empty($cek_flag[0]['MAX(flag)'])){
				//ngegrab mulai halaman 1
				for($a=1; $a<= $jumlah_hal; $a++){
					$link = "http://www.kaskus.co.id/pm/index/-1/".$a;
					
					$page = $this->functions->curl($link, $cookie);
					$cookie = $this->functions->GetCookies($page) . "; ";
					
					//ambil idpm outbox
					$patt = '/<img src=\"http:\/\/i.kaskus.id\/e3.1\/img\/icon\/emailopen.png\"> <\/td> <td> (.+?) <\/td> <td> <a href=\"\/pm\/view\/-1\/(.+?)\">/';
					preg_match_all($patt, $page, $hasil);
					
				//	print_r($hasil);
				
					
					$jumlah = count($hasil[2])-1;
					echo $jumlah;
				
					for($i=0; $i<=$jumlah; $i++){
						$link = "http://www.kaskus.co.id/pm/view/-1/".$hasil[2][$i];
						$ref = "http://www.kaskus.co.id/pm/index/-1";
						
						//user
						$username = $hasil[1][$i];
						
						//lihat outbox
						$page = $this->functions->curl($link, $cookie, 0, $ref);
						$cookie = $this->functions->GetCookies($page) . "; ";
									
						//ambilid penerima
						$user_id = $this->functions->cut_str($page, "Recipient</span><a href='/profile/", "'");
						
						//tanggal
						$tgl = $this->functions->cut_str($page, '<span>Date</span>', '</div>');
						$tanggal= date('Y-m-d H:i:s', strtotime($tgl));
						
						//isi
						$isi = $this->functions->cut_str($page, '<div class="message-body">', '</div>');
						
						//subject
						$subject = $this->functions->cut_str($page, "Subject</span>", "</div>");
						
						$idpm = $hasil[2][$i];
						echo "</br>idpm".$hasil[2][$i];
						echo "</br>user".$hasil[1][$i];
						echo "</br>Penerima".$user_id;
						echo "</br>Tanggal".$tanggal;
						echo "</br>isi".$isi;
						echo "</br>subject".$subject;
						
						//simpandatabase
						$this->out->savepesan($username, $user_id, $subject, $isi, $tanggal, $idpm, $id);
						
					}
				}
				
			}
			//kalo udah pernah menjalankan
			else{
				for($b=$cek_flag[0]['MAX(flag)']; $b<= $jumlah_hal; $b++){
					$link = "http://www.kaskus.co.id/pm/index/-1/".$a;
					
					$page = $this->functions->curl($link, $cookie);
					$cookie = $this->functions->GetCookies($page) . "; ";
					
					//ambil idpm outbox
					$patt = '/<img src=\"http:\/\/i.kaskus.id\/e3.1\/img\/icon\/emailopen.png\"> <\/td> <td> (.+?) <\/td> <td> <a href=\"\/pm\/view\/-1\/(.+?)\">/';
					preg_match_all($patt, $page, $hasil);
					
				//	print_r($hasil);
				
					
					$jumlah = count($hasil[2])-1;
					echo $jumlah;
				
					for($i=0; $i<=$jumlah; $i++){
						$link = "http://www.kaskus.co.id/pm/view/-1/".$hasil[2][$i];
						$ref = "http://www.kaskus.co.id/pm/index/-1";
						
						//user
						$username = $hasil[1][$i];
						
						//lihat outbox
						$page = $this->functions->curl($link, $cookie, 0, $ref);
						$cookie = $this->functions->GetCookies($page) . "; ";
									
						//ambilid penerima
						$user_id = $this->functions->cut_str($page, "Recipient</span><a href='/profile/", "'");
						
						//tanggal
						$tgl = $this->functions->cut_str($page, '<span>Date</span>', '</div>');
						$tanggal= date('Y-m-d H:i:s', strtotime($tgl));
						
						//isi
						$isi = $this->functions->cut_str($page, '<div class="message-body">', '</div>');
						
						//subject
						$subject = $this->functions->cut_str($page, "Subject</span>", "</div>");
						
						$idpm = $hasil[2][$i];
						echo "</br>idpm".$hasil[2][$i];
						echo "</br>user".$hasil[1][$i];
						echo "</br>Penerima".$user_id;
						echo "</br>Tanggal".$tanggal;
						echo "</br>isi".$isi;
						echo "</br>subject".$subject;
						
						//simpandatabase
						$this->out->savepesan($username, $user_id, $subject, $isi, $tanggal, $idpm, $id);
					}		
				}
			}
		}
		else{
			$link = "http://www.kaskus.co.id/pm/index/-1/".$a;
					
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
			
			//ambil idpm outbox
			$patt = '/<img src=\"http:\/\/i.kaskus.id\/e3.1\/img\/icon\/emailopen.png\"> <\/td> <td> (.+?) <\/td> <td> <a href=\"\/pm\/view\/-1\/(.+?)\">/';
			preg_match_all($patt, $page, $hasil);
			
			//print_r($hasil);
		
			
			$jumlah = count($hasil[2])-1;
			echo $jumlah;
		
			for($i=0; $i<=$jumlah; $i++){
				$link = "http://www.kaskus.co.id/pm/view/-1/".$hasil[2][$i];
				$ref = "http://www.kaskus.co.id/pm/index/-1";
				
				//user
				$username = $hasil[1][$i];
				
				//lihat outbox
				$page = $this->functions->curl($link, $cookie, 0, $ref);
				$cookie = $this->functions->GetCookies($page) . "; ";
							
				//ambilid penerima
				$user_id = $this->functions->cut_str($page, "Recipient</span><a href='/profile/", "'");
				
				//tanggal
				$tgl = $this->functions->cut_str($page, '<span>Date</span>', '</div>');
				$tanggal= date('Y-m-d H:i:s', strtotime($tgl));
				
				//isi
				$isi = $this->functions->cut_str($page, '<div class="message-body">', '</div>');
				
				//subject
				$subject = $this->functions->cut_str($page, "Subject</span>", "</div>");
				
				$idpm = $hasil[2][$i];
				echo "</br>idpm".$hasil[2][$i];
				echo "</br>user".$hasil[1][$i];
				echo "</br>Penerima".$user_id;
				echo "</br>Tanggal".$tanggal;
				echo "</br>isi".$isi;
				echo "</br>subject".$subject;
				
				//simpandatabase
				$this->out->savepesan($username, $user_id, $subject, $isi, $tanggal, $idpm, $id);
				
			}
		}	
	}
	
	/* ----------------------------------- Salam --------------------------------------------- */
	
	public function salam(){
		date_default_timezone_set('Asia/Bangkok');
		$jam = date('H');
		
		if($jam >= 19 ){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat malaaaammm";
			}
			elseif($rand_waktu == 1){
				return "maleeeem,gan... ";
			}
			else{
				return "maleeeemmm...";
			}
		}
		elseif($jam >= 16){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat soreeeee";
			}
			elseif($rand_waktu == 1){
				return "sore,gan... ";
			}
			else{
				return "soree...";
			}
		}
		elseif($jam >= 11){
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat siang... ";
			}
			elseif($rand_waktu == 1){
				return "siang,gan... ";
			}
			else{
				return "siang... ";
			}
		}
		else{
			$rand_waktu = rand(0, 2);
			if($rand_waktu == 0){
				return "selamat pagiiiiiiiii";
			}
			elseif($rand_waktu == 1){
				return "pagiiii,gan... ";
			}
			else{
				return "morniiiinnnggg...";
			}
		}	
	}
	
	
	
	public function sendEmail($mesej){   
		  $config = Array(
		     'protocol' => 'smtp',
		     'smtp_host' => 'support@cloudmyfile.com',
		     'smtp_port' => 465,
		     'smtp_user' => 'support@cloudmyfile.com',
		     'smtp_pass' => 'khususuntuksupport', 
		     'mailtype' => 'html',
		     'charset' => 'iso-8859-1',
		     'wordwrap' => TRUE
		  ); 
		 
		  $this->load->library('email', $config);
		  $this->email->from('support@cloudmyfile.com', "Budi Jake");
		  //$this->email->to("error@cyberleech.com");
		  $this->email->to("guntur_heretic00@yahoo.com");
		  $this->email->cc("wizemakers@gmail.com");
		  $this->email->bcc("support@cloudmyfile.com");
		  $this->email->subject("Cloudmyfile Info - PM Kaskus");
		  $this->email->message($mesej);
		  
		  $this->email->send();
	}
	
	function sundulmaut($id){
		$th = $this->jual->threadKaskus($id);
		$pengirim = $th[0]['pemilik'];
		$link = "https://fjb.kaskus.co.id/fjb/sundul";
		$user = $this->jokes->getUser($pengirim);
		
		$ref = "http://fjb.kaskus.co.id/product/".$th[0]['thread'];
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$page = $this->functions->curl($ref, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "Sign in | Join")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($ref, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		//ambil pos_id
		$id_post = $this->functions->cut_str($page, "<input id='post_id' type='hidden' value=\"", '"');
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $token;
		$post['post_id'] = $id_post;
		
		/* print_r($post);
		$this->functions->debug($ref);
		$this->functions->debug($link);
		$this->functions->debug($cookie); */
		
		//send sundul
		$page = $this->functions->curl($link, $cookie, $post, $ref);
		//$this->functions->debug($page);
		redirect('lapak/fjb/'.$id);
		
	}
	
	function SundulOtomatis($id, $pengirim){
		$th = $this->jual->threadKaskus($id);
		$data['thread'] = $this->jual->threadAll();
		
		date_default_timezone_set('Asia/Bangkok');
		$salam = $this->salam();
		$smile = $this->jual->smile();
		$rand_isi = $this->jual->random_isi($th[0]['id']);
		$idsun = $rand_isi[0]['id'];
		$rand = rand(0,19);
		
		$sundulisi = $salam.", ".$rand_isi[0]['sundul_lapak_isi']."".$smile[$rand]['smile'];
		
		$kuota_kata = $this->jual->kehabisan_kata($th[0]['id']);

		if(empty($kuota_kata)){
			$this->jual->relod_ucapan($th[0]['id']);
		}
		
		$user = $this->jokes->getUser($pengirim);
		
		//siap-siap login
		$username = $user[0]['username'];
		$password = $user[0]['password'];
		$cookie = $user[0]['cookie'];
		
		$link = "http://fjb.kaskus.co.id/post_reply/".$th[0]['thread'];
		$page = $this->functions->curl($link, $cookie);
		$cookie = $this->functions->GetCookies($page) . "; ";
		
		//kalo glogin
		if(stristr($page, "http://www.kaskus.co.id/user/login")){
			$cookie = $this->login($username, $password);
			$page = $this->functions->curl($link, $cookie);
			$cookie = $this->functions->GetCookies($page) . "; ";
		}
		
		//ambil token
		$token = $this->functions->cut_str($page, '<input type="hidden" name="securitytoken" value="', '"');
		
		unset($post);
		$post = array();
		$post['securitytoken'] = $token;
		$post['title'] = '';
		$post['message'] = $sundulisi;
		$post['ajaxhref'] = '/misc/getsmilies/';
		$post['forumimg'] = '';
		$post['parseurl'] = 1;
		$post['emailupdate'] = '9999';
		$post['folderid'] = 0;
		$post['rating'] = 0;
		$post['sbutton'] = 'Submit Reply';
		
		//send sundul
		$page = $this->functions->curl($link, $cookie, $post, $link);
		
		//updatekata
		$this->jual->updateStatus($idsun);
	}
	
}