<?php
	Class Admin_model extends CI_Model {
		function getThreadAll(){
			return $this->db->get('thread_kaskus')->result_array();
		}
		function getThreadId($id){
			return $this->db->get_where('thread_kaskus', array('thread_id'=>$id))->result_array();
		}
		function simpanThread($linkthread){
			$data = array(
				'thread_link'=>$linkthread,
				'keterangan'=>$this->input->post('keterangan')
			);
			if($this->input->post('id_thread')){
				$this->db->where('thread_id', $this->input->post('id_thread'));
				$this->db->update('thread_kaskus', $data);
			}
			else{
				$this->db->insert('thread_kaskus', $data);
			}
		}
		
		function deleteThread($id){
			$this->db->where('thread_id', $id);
			$this->db->delete('thread_kaskus');
		}
		
		function getSundulTemp(){
			return $this->db->get('sundul_cmf')->result_array();
		}
		
		function getSundulcmfId($id){
			return $this->db->get_where('sundul_cmf', array('sundul_cmf_id'=>$id))->result_array();
		}
		function deleteSundulCmf($id){
			$this->db->where('sundul_cmf_id', $id);
			$this->db->delete('sundul_cmf');
		}
		function simpanSundulCmf(){
			if($this->input->post('status') != ''){
				$status = $this->input->post('status');
			}
			else{
				$status = 0;
			}
			
			$data = array(
				'sundul_cmf_isi'=>$this->input->post('tempsundul'),
				'sundul_status'=>$status
			);
			if($this->input->post('id_sundulcmf')){
				$this->db->where('sundul_cmf_id', $this->input->post('id_sundulcmf'));
				$this->db->update('sundul_cmf', $data);
			}
			else{
				$this->db->insert('sundul_cmf', $data);
			}
		}
		
		function getUser(){
			return $this->db->get('user_kaskus')->result_array();
		}
		function getUserId($id){
			return $this->db->get_where('user_kaskus', array('user_id'=>$id))->result_array();
		}
		function simpanUser($cookie){
			
			$arrnya = array();
			$arrnya['tanggal'] = date('Y-m-d');
			$arrnya['password'] = $this->input->post('password');
			
			$hasilenkrip = $this->functions->encryptkhusus($arrnya);
			
			$data = array(
				'username'=>$this->input->post('username'),
				'password'=>$hasilenkrip,
				'cookie'=>$cookie
			);
			
			$this->db->insert('user_kaskus', $data);
		}
		function cekPassword($uid, $pass){
			return $this->db->get_where('user_kaskus', array('user_id'=>$uid))->result_array();
		}
		function deleteUser($id){
			$this->db->where('user_id', $id);
			$this->db->delete('user_kaskus');
		}
	}