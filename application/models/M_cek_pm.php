<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_cek_pm extends CI_Model {
	
	function savepesan($username, $user_id, $subject, $isi, $tanggal, $idpm, $id, $isipesan){
		$simpan = $this->db->query("INSERT IGNORE INTO cek_pm (username, user_id, subject, isi, tanggal, id_pmasli, stat, penerima, isipesan) VALUES ('$username', '$user_id', '$subject', '$isi', '$tanggal', '$idpm', 0, '$id', '$isipesan')");
		return $simpan;		
	}
	
	function tampil(){
		$semua = $this->db->query("select * from cek_pm order by tanggal desc");
		return $semua->result_array();
	}
	
	function updateStatusLink($id){
			$data = array(
				'stat'=> 1
			);
			$this->db->where('id_pm', $id);
			$this->db->update('cek_pm', $data);
	}
	
	function updateHapus($id){
			$data = array(
				'stat'=> 2
			);
			$this->db->where('id_pm', $id);
			$this->db->update('cek_pm', $data);
	}
	
	function lihatemailsalahsatu($id){
		$baru= $this->db->query("select * from cek_pm where penerima = '$id'");
		return $baru->result_array();
	}
	
	function detail($id){
		$baru= $this->db->query("select * from cek_pm where id_pmasli = '$id'");
		return $baru->result_array();
	}
	
	function pesandihapussetjo(){
		$baru= $this->db->query("select * from cek_pm where penerima = 3 AND stat = 2");
		return $baru->result_array();
	}
	
	function pesandihapusrollman(){
		$baru= $this->db->query("select * from cek_pm where penerima = 2 AND stat = 2");
		return $baru->result_array();
	}
	
	function cek_user(){
		$semua = $this->db->query("select * from user_kaskus");
		return $semua->result_array();
	}
	
	function pesan_salahsatu($id){
		$baru= $this->db->query("select * from cek_pm where penerima = '$id' AND stat = 0");
		return $baru->result_array();
	}
	
	function pesan(){
		$baru= $this->db->query("select * from cek_pm where stat = 0");
		return $baru->result_array();
	}
	
	function pesandibaca(){
		$baru= $this->db->query("select * from cek_pm where stat = 1");
		return $baru->result_array();
	}
	
	function pesandihapus(){
		$baru= $this->db->query("select * from cek_pm where stat = 2");
		return $baru->result_array();
	}
}