<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lapak extends CI_Model {

	public $db_thread = 'thread_junk';
	
	function threadKaskus($id){
		$thread = $this->db->query("SELECT * FROM thread_kaskus WHERE thread_id = '$id'");
		return $thread->result_array();
	}
	
	function pengguna_cmf(){
		$semua = $this->db->query("SELECT * FROM  user_kaskus WHERE user_id =2 OR user_id =4");
		return $semua->result_array();
	}
	
	function getUser($id){
			return $this->db->get_where('user_kaskus', array('user_id'=>$id))->result_array();		
	}
	
	function link($id){
		$update = $this->db->query("select * from thread_kaskus where thread id < 5");
		return $update;
	}
	
	function saveCookie($user, $cookie){
			$data = array(
				'cookie'=> $cookie
			);
			$this->db->where('username', $user);
			$this->db->update('user_kaskus', $data);
	}
	
	public function random_isi(){
		$random = $this->db->query("SELECT id, sundul_lapak_isi FROM sundul_lapak_cl WHERE sundul_lapak_status = 0 ORDER BY RAND() LIMIT 1");
		return $random->result_array();
	}
	
	function updateStatuscl($id){
			$data = array(
				'sundul_lapak_status'=> 1
			);
			$this->db->where('id', $id);
			$this->db->update('sundul_lapak_cl', $data);
	}
	
	function updateStatuscmf($id){
			$data = array(
				'sundul_lapak_status'=> 1
			);
			$this->db->where('id', $id);
			$this->db->update('sundul_lapak_cmf', $data);
	}
	
	public function random_isi_cmf(){
		$random = $this->db->query("SELECT id, sundul_lapak_isi FROM sundul_lapak_cmf WHERE sundul_lapak_status = 0 ORDER BY RAND() LIMIT 1");
		return $random->result_array();
	}
	
	function smile(){
		$random = $this->db->query("SELECT smile FROM smile ORDER BY RAND()");
		return $random->result_array();
	}
	
	function kehabisan_kata(){
		$habis = $this->db->query("SELECT * FROM sundul_lapak_cl WHERE sundul_lapak_status = 0");
		return $habis->result_array();
	}
	
	function kehabisan_kata_cmf(){
		$habis = $this->db->query("SELECT * FROM sundul_lapak_cmf WHERE sundul_lapak_status = 0");
		return $habis->result_array();
	}
	
	function relod_ucapan(){
		$update = $this->db->query("update sundul_lapak_cl set sundul_lapak_status = 0");
		return $update;
	}
	
	function relod_ucapan_cmf(){
		$update = $this->db->query("update sundul_lapak_cmf set sundul_lapak_status = 0");
		return $update;
	}
	
	function add_cl(){
		$st = array(
			'sundul_lapak_isi' => $this->input->post('sundulan'),
			'sundul_lapak_status' => 0,
        );
        $this->db->insert('sundul_lapak_cl', $st);
	}
	
	function add_cmf(){
		$st = array(
			'sundul_lapak_isi' => $this->input->post('sundulan'),
			'sundul_lapak_status' => 0,
        );
        $this->db->insert('sundul_lapak_cmf', $st);
	}
	
	function edit_cl($id){
		$data = array(
			'sundul_lapak_isi'=> $this->input->post('sundulan'),
		);
		$this->db->where('id', $id);
		$this->db->update('sundul_lapak_cl', $data);
	}
	
	function edit_cmf($id){
		$data = array(
			'sundul_lapak_isi'=> $this->input->post('sundulan'),
		);
		$this->db->where('id', $id);
		$this->db->update('sundul_lapak_cmf', $data);
	}
	
	function sundulancl(){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_cl");
		return $sundul->result_array();
	}
	
	function editsundulancl($id){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_cl WHERE id='$id'");
		return $sundul->result_array();
	}
	
	function editsundulancmf($id){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_cmf WHERE id='$id'");
		return $sundul->result_array();
	}
	
	function sundulancmf(){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_cmf");
		return $sundul->result_array();
	}
	
	function deletecl($id){
		$this->db->where('id', $id);
		$this->db->delete('sundul_lapak_cl');
	}
	
	function deletecmf($id){
		$this->db->where('id', $id);
		$this->db->delete('sundul_lapak_cmf');
	}
	/* ------------------------------------------------ flux dan pass------------------------------------------------------------------- */
	
	public function random_isi_pass(){
		$random = $this->db->query("SELECT id, sundul_lapak_isi FROM sundul_lapak_pass WHERE sundul_lapak_status = 0 ORDER BY RAND() LIMIT 1");
		return $random->result_array();
	}
	
	function updateStatuspass($id){
			$data = array(
				'sundul_lapak_status'=> 1
			);
			$this->db->where('id', $id);
			$this->db->update('sundul_lapak_cl', $data);
	}
	
	function updateStatusflux($id){
			$data = array(
				'sundul_lapak_status'=> 1
			);
			$this->db->where('id', $id);
			$this->db->update('sundul_lapak_cmf', $data);
	}
	
	public function random_isi_flux(){
		$random = $this->db->query("SELECT id, sundul_lapak_isi FROM sundul_lapak_flux WHERE sundul_lapak_status = 0 ORDER BY RAND() LIMIT 1");
		return $random->result_array();
	}
	
	function kehabisan_kata_flux(){
		$habis = $this->db->query("SELECT * FROM sundul_lapak_flux WHERE sundul_lapak_status = 0");
		return $habis->result_array();
	}
	
	function kehabisan_kata_pass(){
		$habis = $this->db->query("SELECT * FROM sundul_lapak_pass WHERE sundul_lapak_status = 0");
		return $habis->result_array();
	}
	
	function relod_ucapan_pass(){
		$update = $this->db->query("update sundul_lapak_pass set sundul_lapak_status = 0");
		return $update;
	}
	
	function relod_ucapan_flux(){
		$update = $this->db->query("update sundul_lapak_flux set sundul_lapak_status = 0");
		return $update;
	}
	
	function add_pass(){
		$st = array(
			'sundul_lapak_isi' => $this->input->post('sundulan'),
			'sundul_lapak_status' => 0,
        );
        $this->db->insert('sundul_lapak_pass', $st);
	}
	
	function add_flux(){
		$st = array(
			'sundul_lapak_isi' => $this->input->post('sundulan'),
			'sundul_lapak_status' => 0,
        );
        $this->db->insert('sundul_lapak_flux', $st);
	}
	
	function edit_pass($id){
		$data = array(
			'sundul_lapak_isi'=> $this->input->post('sundulan'),
		);
		$this->db->where('id', $id);
		$this->db->update('sundul_lapak_pass', $data);
	}
	
	function edit_flux($id){
		$data = array(
			'sundul_lapak_isi'=> $this->input->post('sundulan'),
		);
		$this->db->where('id', $id);
		$this->db->update('sundul_lapak_flux', $data);
	}
	
	function sundulanflux(){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_flux");
		return $sundul->result_array();
	}
	
	function editsundulanpass($id){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_pass WHERE id='$id'");
		return $sundul->result_array();
	}
	
	function editsundulanflux($id){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_flux WHERE id='$id'");
		return $sundul->result_array();
	}
	
	function sundulanpass(){
		$sundul = $this->db->query("SELECT * FROM sundul_lapak_pass");
		return $sundul->result_array();
	}
	
	function deletepass($id){
		$this->db->where('id', $id);
		$this->db->delete('sundul_lapak_pass');
	}
	
	function deleteflux($id){
		$this->db->where('id', $id);
		$this->db->delete('sundul_lapak_flux');
	}
	
}