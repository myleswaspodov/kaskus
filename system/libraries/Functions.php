<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Functions{
	
	function geturl($host, $port, $url, $referer = 0, $cookie = 0, $post = 0, $saveToFile = 0, $proxy = 0, $pauth = 0, $auth = 0, $scheme = "http", $resume_from = 0, $XMLRequest=0) {
		global $nn, $lastError, $PHP_SELF, $AUTH, $IS_FTP, $FtpBytesTotal, $FtpBytesReceived, $FtpTimeStart, $FtpChunkSize, $Resume, $bytesReceived, $fs, $forbidden_filetypes, $rename_these_filetypes_to, $bw_save, $force_name, $rename_prefix, $rename_suffix, $limitsize, $lowlimitsize, $limitbyip, $ipmu, $ada_acc, $pointboost, $add_ext_5city, $htxt, $gtxt, $mip_enabled, $storage_limit, $koderesellernya, $servercyberleech;
		$nn = "\r\n";
		
		$scheme.= "://";

		if (($post !== 0) && ($scheme == "http://" || $scheme == "https://")) {
			$method = "POST";
			$postdata = is_array($post) ? formpostdata($post) : $post;
			$length = strlen($postdata);
			$content_tl = "Content-Type: application/x-www-form-urlencoded" . $nn . "Content-Length: " . $length . $nn;
		} else {
			$method = "GET";
			$postdata = "";
			$content_tl = "";
		}

		$cookies = '';
		if ($cookie) {
			if (is_array($cookie)) {
				$cookies = "Cookie: " . CookiesToStr($cookie) . $nn;
			} else {
				$cookies = "Cookie: " . $cookie . $nn;
			}
		}
		$referer = $referer ? "Referer: " . $referer . $nn : "";

		if ($scheme == "https://") {
			$scheme = "ssl://";
			$port = 443;
		}
		
		$host = $host . ($port != 80 && $port != 443 ? ":" . $port : "");
		
		if ($proxy) {
			list($proxyHost, $proxyPort) = explode(":", $proxy);
			$url = $scheme . $host . $url;
		} else {
			$proxyHost = '';
			$proxyPort = '';
		}

		if ($scheme != "ssl://") {
			$scheme = "";
		}

		$http_auth = ($auth) ? "Authorization: Basic " . $auth . $nn : "";
		$proxyauth = ($pauth) ? "Proxy-Authorization: Basic " . $pauth . $nn : "";

		$request = $method . " " . str_replace(" ", "%20", $url) . " HTTP/1.1" . $nn;
		$request .= "Host: " . $host . $nn;
		$request .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.14) Gecko/20080404 Firefox/2.0.0.14" . $nn;
		$request .= "Accept: */*" . $nn;
		$request .= "Accept-Language: en-us;q=0.7,en;q=0.3" . $nn;
		$request .= "Accept-Charset: utf-8,windows-1251;q=0.7,*;q=0.7" . $nn;
		$request .= "Pragma: no-cache" . $nn;
		$request .= "Cache-Control: no-cache" . $nn;
		$request .= ($Resume ["use"] === TRUE ? "Range: bytes=" . $Resume ["from"] . "-" . $nn : "");
		$request .= $http_auth;
		$request .= $proxyauth;
		$request .= $referer;
		$request .= ($XMLRequest ? "X-Requested-With: XMLHttpRequest" . $nn : "");
		$request .= $cookies;
		$request .= "Connection: Close" . $nn;
		$request .= $content_tl . $nn;
		$request .= $postdata;


	//write_file(CONFIG_DIR."request.txt", $request, 0); // add

		$errno = 0;
		$errstr = "";
		$hosts = ($proxyHost ? $scheme . $proxyHost : $scheme . $host) . ':' . ($proxyPort ? $proxyPort : $port);
		$fp = @stream_socket_client($hosts, $errno, $errstr, 120, STREAM_CLIENT_CONNECT);
		


		if (!$fp) {
			$dis_host = $proxyHost ? $proxyHost : $host;
			$dis_port = $proxyPort ? $proxyPort : $port;
			echo "cantconnect";exit();
		}

		if ($errno || $errstr) {
			$lastError = $errstr;
			return false;
		}

	#########################################################################
		//echo "<br><br>| ".$request." |<br><br>";
		fputs($fp, $request);
		fflush($fp);
		$timeStart = getmicrotime();

		$header = '';
		do {
			$header .= fgets($fp, 16384);
		} while (strpos($header, $nn . $nn) === false);

	#########################################################################
		//echo "<br><br> |".$header." |<br><br>";

		if (!$header) {
			$lastError = "No header received";
			return false;
		}

		$responsecode = "";
		preg_match('/^HTTP\/1\.0|1 ([0-9]+) .*/', $header, $responsecode);
		if (($responsecode[1] == 404 || $responsecode[1] == 403) && $saveToFile) {
			// end of fungsi ini di copy terus, soalnya biar bisa nampilin instant request dengan baik
			// Do some checking, please, at least tell them what error it was
			if ($responsecode[1] == 403) {
				$lastError = 'The page was not found!';
			} elseif ($responsecode[1] == 404) {
				$lastError = 'You are forbidden to access the page!';
			} else {
				// Weird, it shouldn't come here...
				$lastError = 'The page was either forbidden or not found!';
			}
			return false;
		}
	//write_file(CONFIG_DIR."header.txt", $header);

		$page = $header;

		do {
			$data = @fread($fp, ($saveToFile ? $chunkSize : 16384));
			if ($data == '') break;
			if ($saveToFile) {
				$bytesSaved = fwrite($fs, $data);
				if ($bytesSaved > -1) {
					$bytesReceived += $bytesSaved;
				} else {
					$lastError = "It is not possible to carry out a record in the file " . $saveToFile;
					return false;
				}
				if ($bytesReceived >= $bytesTotal) {
					$percent = 100;
				} else {
					$percent = @round(($bytesReceived + $Resume["from"]) / ($bytesTotal + $Resume["from"]) * 100, 2);
				}
				if ($bytesReceived > $last + $chunkSize) {
					$received = bytesToKbOrMbOrGb($bytesReceived + $Resume["from"]);
					$time = getmicrotime() - $timeStart;
					$chunkTime = $time - $lastChunkTime;
					$chunkTime = $chunkTime ? $chunkTime : 1;
					$lastChunkTime = $time;
					$speed = @round($chunkSize / 1024 / $chunkTime, 2);
					echo "<script type='text/javascript'>pr('" . $percent . "', '" . $received . "', '" . $speed . "');</script>";
					$last = $bytesReceived;
				}
			} else {
				$page.= $data;
			}
		} while (strlen($data) > 0);

		fclose($fp);
		
		return $page;
	}
	
	function CookiesToStr($cookie=array()) {
		$cookies = "";
		foreach ($cookie as $k => $v) {
			$cookies .= "$k=$v;";
		}
		// Remove the last ';'
		//$cookies = substr($cookies, 0, -1);
		return $cookies;
	}

	function GetCookies($content) {
		// The U option will make sure that it matches the first character
		// So that it won't grab other information about cookie such as expire, domain and etc
		preg_match_all('/Set-Cookie: (.*)(;|\r\n)/U', $content, $temp);
		$cookie = $temp[1];
		$cook = implode('; ', $cookie);
		return $cook;
	}

	function GetCookiesArr($content, $del=true, $dval='deleted') {
		preg_match_all('/Set-Cookie: (.*)(;|\r\n)/U', $content, $temp);
		$cookie = array();
		foreach ($temp[1] as $v) {
			$v = explode('=', $v, 2);
			$cookie[$v[0]] = $v[1];
			if ($del && $v[1] == $dval) unset($cookie[$v[0]]);
		}
		return $cookie;
	}

	function GetCookiesAll($page, $cookie=0) {
		$cookiearr = GetCookiesArr($page);
		if($cookie)
			$cookiebaru = array_merge($cookie, $cookiearr);
		else
			$cookiebaru = $cookiearr;
		return $cookiebaru;
	}

	function cut_str($str, $left, $right) {
		$str = substr ( stristr ( $str, $left ), strlen ( $left ) );
		$leftLen = strlen ( stristr ( $str, $right ) );
		$leftLen = $leftLen ? - ($leftLen) : strlen ( $str );
		$str = substr ( $str, 0, $leftLen );
		return $str;
	}

	// thx to http://forrst.com/posts/Check_if_a_string_is_serialized_data_with_PHP-YLB
	function is_serialized($data) {
		// if it isn't a string, it isn't serialized
		if(!is_string($data))
			return false;
		$data = trim($data);
		if('N;' == $data)
			return true;
		if(!preg_match('/^([adObis]):/', $data, $badions))
			return false;
		switch ($badions[1]) {
			case 'a' :
			case 'O' :
			case 's' :
				if(preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data))
					return true;
				break;
			case 'b' :
			case 'i' :
			case 'd' :
				if(preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data))
					return true;
				break;
		}
		return false;
	}


	function debug($page, $varay=false){
		echo "<br /><textarea cols='120' rows='30'>" . $page . "</textarea><br />";
		if($varay) print_r($varay);
	}
	
	function GetPage($link, $cookie = 0, $post = 0, $referer = 0, $auth = 0, $XMLRequest=0) {
		$link = html_entity_decode($link);
		
		$Url = parse_url(trim($link));
		$page = geturl($Url ["host"], $Url ["port"] ? $Url ["port"] : 80, $Url ["path"] . ($Url ["query"] ? "?" . $Url ["query"] : ""), $referer, $cookie, $post, 0, $_GET ["proxy"], $pauth, $auth, $Url ["scheme"], 0, $XMLRequest);
		
		return $page;
	}

	function curl($url, $cookies=0, $post=0, $referrer=0, $XMLRequest=0, $header=1, $proxyport=0) {	
		$url = html_entity_decode($url);
		
		$ch = @curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, $header);
		if ($cookies) {
			if (is_array($cookies)) {
				$cookies = CookiesToStr($cookies);
			}
			curl_setopt($ch, CURLOPT_COOKIE, $cookies);	
		}
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');
		if ($XMLRequest) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest"));
		}
		if($proxyport) {
			curl_setopt($ch, CURLOPT_PROXY, $proxyport);
		}
		curl_setopt($ch, CURLOPT_REFERER, $referrer); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($post){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
		$page = curl_exec( $ch);
		curl_close($ch);
		
		return $page;
	}

	function readcookie($cookiepath) {
		$filename = $cookiepath;
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		if(is_serialized($contents)) {
			$contents = unserialize($contents);
		}
		fclose($handle);
		return $contents;
	}

	function writecookie($cookiepath, $cookievalue) {
		$myFile = $cookiepath;
		$fh = fopen($myFile, 'w') or die("Can't open specified cookie path");
		if (is_array($cookievalue)) {
			$cookievalue = serialize($cookievalue);
		}
		fwrite($fh, $cookievalue);
		fclose($fh);
	}
	
	function safe_b64encode($string) {
		$data = base64_encode($string);
		$data = str_replace(array('+','/','='),array('-','_',''),$data);
		return $data;
	}

	function safe_b64decode($string) {
		$data = str_replace(array('-','_'),array('+','/'),$string);
		$mod4 = strlen($data) % 4;
		if ($mod4) {
			$data .= substr('====', $mod4);
		}
		return base64_decode($data);
	}

	function encryptkhusus($value) {
		$skey = "1a5ew2177";
		if(!$value){return false;}
		$text = serialize($value);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
		return trim($this->safe_b64encode($crypttext)); 
	}

	function decryptkhusus($value) {
		$skey = "1a5ew2177";
		if(!$value){return false;}
		$crypttext = $this->safe_b64decode($value); 
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
		return unserialize(trim($decrypttext));
	}
	
	public function enterrecaptcha($linkrecaptcha=0, $imgfilename, $variablenya, $post=0, $pesan=0) {
		global $nn, $download_dir;		
		
		if($linkrecaptcha) {
			$cook = 'PREF=ID=1111111111111111:FF=0:LD=en:TM=1435567280:LM=1435568580:GM=1:V=1:S=eGrokYKawxBOjBFN; NID=68=alxsX-AnNP-swf98ugdTzcpOglfvh3H0CRU3HZ4-I0ojUICPgdh0Dyzz_u_vI1JprvdO3GdssmdPD_1Qut2mHhc3_eiVptDPcND5Vu9GP9cJOEMuPIDqKe7CsVDk4TOlXGRNW1ZRTZ-M4D30dkswTE-OHzBU_aV4; OGP=-5061451:; OGPC=5061617-3:; SID=DQAAAC4BAAA0fjIjFv5IOsxCPG0XTiZqHOjnLud7SbYcS0TIoQXhF6bpckNaNOqNAnX9R3T0yPNH02GnxhwdGqL0xuRiyiCBxuQKuBmapInNcGZX1XG4XyCm6LI5TrcIlPi6wGAa_5SV106PSTeRVMrzLM52obdliZmn_4-wdKWFtXN9OnJtVeRKDNIazrxvyHS6D_Z2kPhT1O4fkgsOYAi9XJ-DcZ7B_UeoW2jJ_1qU1iHsYaN8RvuZ-FOqqJPAc0GrDJ9YEQDcdE_YPS9R0XJcoqN5eB0bkhSBLz3QbITcMWkiS5xU2SDaz2wRjWEXucED4OCH_f0sWwuUNmB8jGroG499YzcsFjE9niaaZJ8aQRp6WvWtvg7wGvJURAZm0bbwRGo7OLOwddrUmp1Z2INugkxFHz32; HSID=A_p0BKHH_s6LAz1yv; APISID=53QNHyVUd69xwkel/APHpMtb7gXmPOEPcy
';
			$page = $this->GetPage($linkrecaptcha, $cook);
			is_present($page, "Expired session", "Recaptcha ERROR - Expired session");
			$ch = cut_str($page, "challenge : '", "'");
			$page = $this->GetPage("http://www.google.com/recaptcha/api/image?c=".$ch);
			$headerend = strpos($page,"\r\n\r\n");
			$pass_img = substr($page,$headerend+4);
			$imgfile = $download_dir.$imgfilename;
			$fimg = fopen($imgfile,"w");
			fwrite($fimg,$pass_img);
			fclose($fimg);

			$recaptcha_challenge_field = $ch;
		}
		else {
			$imgfile = $imgfilename;
		}
		
		
		$code = '<center>';
		if($pesan) {
			$code .= '<BR/>'.$pesan.'<BR/>'.$nn.$nn;
		}
		$code .= '<BR/><h3 style=\'text-align: center;\'>Silakan masukkan captcha di bawah ini dengan benar :<BR/><BR/>Klo gagal brati anda salah masukkan captcha, silakan leech ulang.<BR/><BR/>'.$nn.$nn;
		
		$code .= '<BR/><img src='.$imgfile.'>'.$nn;
		
		$code .= '<form name="dl" method="post" action="'.$PHP_SELF.(isset($_GET["idx"]) ? "?idx=".$_GET["idx"] : "").'">'.$nn;
		
		foreach ($variablenya as $name => $input) {
            $code .= '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $input . '" />'.$nn;
        }
		
		if($post) {
			$code .= '<input name="post" type="hidden" value="'.urlencode(serialize($post)).'" />'.$nn;
		}
		
		if($linkrecaptcha) {
			$code .= '<input type="hidden" name="recaptcha_challenge_field" value="'.urlencode($recaptcha_challenge_field).'">'.$nn;		
		}
		
		$code .= '<BR/><BR/><input size="30" type="text" name="captcha" value="" autofocus="autofocus" required="required" />'.$nn;
		
		$code .= '<input type="submit" name="action" value="Download" />'.$nn;
		
		$code .= '</form></center>';
		echo ($code) ;		
		
		/*
		// hapus captcha otomatis, kasih sleep biar ga kecepeten hapusnya
		sleep(10);
		if(file_exists($imgfile)) {
			unlink($imgfile);
		}
		*/
		exit();
	}
}